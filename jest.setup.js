import '@testing-library/jest-dom/extend-expect'
import { enableFetchMocks } from 'jest-fetch-mock'
enableFetchMocks()

jest.mock('react-chartjs-2', () => ({
    Line: () => null
  }));