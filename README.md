# Cicada Technologies Challenge.

## Solved by:

- Héctor Remedios [@R-Art-project ](https://gitlab.com/R-Art-project)


## Description

The project is developed in Next.js, Typescript. Redux was implemented to handle the data coming from the API. Jest and React Testing Library are used to run the tests.

## Run the project

Repository: https://gitlab.com/R-Art-project/cicada-app

Clone with SSH:
```bash
git clone git@gitlab.com:R-Art-project/cicada-app.git
```
Clone with HTTPS:
```bash
git clone https://gitlab.com/R-Art-project/cicada-app.git
```

### Installation

```bash
npm install
# or
yarn install
# or
pnpm install
```
### Run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
## Running Tests

To run tests, run the following command
```bash
  npm run test
```
Note: The test in this project is for reference only. But be prepared to stop to handle the redux state and pre-load an initial state.
## Deploy on Vercel
This project is deployed in: [https://cicada-challenge.vercel.app/](https://cicada-challenge.vercel.app/) 

#### Consideration: For the correct visualization of the data, access to the API must be allowed from this endpoint. This must be done by the API creator..
