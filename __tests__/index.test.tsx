import { screen } from "@testing-library/react";
import Home from "@/pages/index";
import { renderWithProviders } from "@/util/test-utils";

describe("Home page", () => {
  it("Testing the render of Home", () => {
    renderWithProviders(<Home />);

    const heading = screen.getByRole("heading", {
      name: "Currency Pair",
    });
    expect(heading).toBeInTheDocument();
  });
});

