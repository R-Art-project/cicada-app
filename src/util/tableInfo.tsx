import {
  ColumnDefinitionType,
  PairData
} from '@/components/atoms/AtomTable/types';

export const columnsHistoricPrice: ColumnDefinitionType<
  PairData,
  keyof PairData
>[] = [
  {
    key: 'date',
    header: 'Date',
    width: 150
  },
  {
    key: 'high',
    header: 'High'
  },
  {
    key: 'low',
    header: 'Low'
  }
];

export const columnsDailyTrends: ColumnDefinitionType<
  PairData,
  keyof PairData
>[] = [
  {
    key: 'date',
    header: 'Date',
    width: 150
  },
  {
    key: 'open',
    header: 'Open'
  },
  {
    key: 'close',
    header: 'Close'
  },
  {
    key: 'difference',
    header: 'Difference'
  }
];
