import React, { PropsWithChildren } from 'react';
import { render } from '@testing-library/react';
import type { RenderOptions } from '@testing-library/react';
import { configureStore } from '@reduxjs/toolkit';
import type { PreloadedState } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';

import type { RootState } from '../app/store';
import pairReducer from '@/features/pairs/pairSlice';
import historicDataReducer from '@/features/historicData/historicDataSlice';
import currentRateReducer from '@/features/currentRaite/currentRaiteSlice';

// This type interface extends the default options for render from RTL, as well
// as allows the user to specify other things such as initialState, store.
interface ExtendedRenderOptions extends Omit<RenderOptions, 'queries'> {
  preloadedState?: PreloadedState<RootState>;
  store?: any;
}

export function renderWithProviders(
  ui: React.ReactElement,
  {
    preloadedState = {
      currentRate: {
        currentRate: {
          EURUSD: [
            {
              point: 125.20786389674228,
              pairId: 'EURUSD',
              time: '4/13/2023, 11:23:11 AM'
            }
          ]
        }
      },
      pairs: {
        selectedpair: {
          id: 'EURUSD',
          label: 'EUR-USD'
        }
      },
      pairData: {
        historcData: {
          timeSeries: [
            {
              open: 0.9687,
              high: 0.97014,
              low: 0.95501,
              close: 0.96672,
              difference: -0.00198,
              date: '2022-09-26'
            }
          ]
        }
      }
    },
    // Automatically create a store instance if no store was passed in
    store = configureStore({
      reducer: {
        currentRate: currentRateReducer,
        pairs: pairReducer,
        pairData: historicDataReducer
      },
      preloadedState
    }),
    ...renderOptions
  }: ExtendedRenderOptions = {}
) {
  function Wrapper({ children }: PropsWithChildren<{}>): JSX.Element {
    return <Provider store={store}>{children}</Provider>;
  }

  // Return an object with the store and all of RTL's query functions
  return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}
