import { persistReducer } from 'redux-persist';
import { configureStore, combineReducers } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';

import pairReducer from '@/features/pairs/pairSlice';
import historicDataReducer from '@/features/historicData/historicDataSlice';
import currentRateReducer from '@/features/currentRaite/currentRaiteSlice';

const persisConfig = {
  key: 'root',
  storage
};

const rootReducer = combineReducers({
  pairs: pairReducer,
  pairData: historicDataReducer,
  currentRate: currentRateReducer
});

const persistedReducer = persistReducer(persisConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (defaultMiddlewaer) =>
    defaultMiddlewaer({
      serializableCheck: false
    })
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
