import { motion } from 'framer-motion';
import styled from '@emotion/styled';
import { theme } from '@/styles/globalstyles';
import { StylesTableProps } from './types';

export const TD = styled(motion.td)<StylesTableProps>`
  background-color: ${theme?.darkMode?.black};
  color: ${({ color }) => color || theme?.darkMode?.white};
  text-align: ${({ align }) => align || 'left'};
  font-family: 'flama';
  padding: 7px;
  border-bottom: 1px solid ${theme?.darkMode?.white};

  @media (max-width: 459px) {
    font-size: 12px;
  }
`;

export const TH = styled(motion.th)<StylesTableProps>`
  text-align: ${({ align }) => align || 'left'};
  font-family: 'flama';
  height: 46px;
  vertical-align: bottom;
  padding: 7px 14px;
  @media (max-width: 459px) {
    font-size: 12px;
  }
`;

export const Tbody = styled(motion.tbody)`
  background-color: ${theme?.darkMode?.black};
  color: ${theme?.darkMode?.white};
  padding: 7px;
`;
