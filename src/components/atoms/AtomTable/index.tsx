import { theme } from '@/styles/globalstyles';
import { css } from '@emotion/react';
import AtomWrapper from '../AtomWrapper';
import TableHeader from './tableHeader';
import TableRows from './tableRows';
import { TableProps } from './types';

export const AtomTable = <T, K extends keyof T>({
  data,
  columns,
  tableTitel
}: TableProps<T, K>): JSX.Element => {
  return (
    <AtomWrapper
      customcss={css`
        width: 100%;
        height: 200px;
        @media (max-width: 970px) {
          margin-bottom: 30px;
        }

        table {
          display: flex;
          flex-flow: column;
          height: 100%;
          width: 100%;
          border-collapse: collapse;
        }
        table thead {
          flex: 0 0 auto;
          width: 100%;
        }
        table tbody {
          flex: 1 1 auto;
          display: block;
          overflow-y: scroll;
        }
        table tbody tr {
          width: 100%;
        }
        table thead,
        table tbody tr {
          display: table;
          table-layout: fixed;
        }
        th {
          border-bottom: ${`1px solid ${theme?.darkMode?.white}`};
          border-spacing: 0;
          color: ${theme?.darkMode?.white};
          background-color: ${theme?.darkMode?.primary};
        }
      `}
    >
      <AtomWrapper
        backgroundColor={theme?.darkMode?.secundary}
        customcss={css`
          height: 50px;
          width: 100%;
          padding: 12px;

          h1 {
            color: ${theme?.darkMode?.white};
            font-family: 'flama';
          }
        `}
      >
        <h1>{tableTitel}</h1>
      </AtomWrapper>
      <table>
        <TableHeader columns={columns} />
        <TableRows data={data} columns={columns} />
      </table>
    </AtomWrapper>
  );
};
