import { theme } from '@/styles/globalstyles';
import { TH } from './styles';
import { ColumnDefinitionType, TableHeaderProps } from './types';

const TableHeader = <T, K extends keyof T>({
  columns
}: TableHeaderProps<T, K>): JSX.Element => {
  const headers = columns.map((column, index) => {
    return (
      <TH
        key={`headCell-${index}`}
        height={`${column.height}px`}
        align={column.key !== 'date' ? 'right' : 'left'}
      >
        {column.header}
      </TH>
    );
  });

  return (
    <thead>
      <tr>{headers}</tr>
    </thead>
  );
};

export default TableHeader;
