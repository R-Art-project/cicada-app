import { theme } from '@/styles/globalstyles';
import { Tbody, TD } from './styles';
import { TableRowsProps } from './types';

const TableRows = <T, K extends keyof T>({
  data,
  columns
}: TableRowsProps<T, K>): JSX.Element => {
  const rows = data?.map((row, index) => {
    return (
      <tr key={`row-${index}`}>
        {columns?.map((column, index2) => {
          return (
            <TD
              key={`cell-${index2}`}
              color={
                column.key === 'difference'
                  ? `${row[column.key]}`.slice(0, 1) === '-'
                    ? theme?.darkMode?.red
                    : theme?.darkMode?.green
                  : theme?.darkMode?.white
              }
              align={column.key !== 'date' ? 'right' : 'left'}
            >
              {`${
                column.key === 'difference'
                  ? `${row[column.key]}`.slice(0, 1) === '-'
                    ? ''
                    : `+`
                  : ''
              }${row[column.key]}`}
            </TD>
          );
        })}
      </tr>
    );
  });

  return <Tbody>{rows}</Tbody>;
};

export default TableRows;
