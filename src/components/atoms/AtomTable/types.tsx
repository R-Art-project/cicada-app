import { MotionProps } from 'framer-motion';

export interface AtomTableTypes extends MotionProps {
  data: any;
}

export type ColumnDefinitionType<T, K extends keyof T> = {
  key: K;
  header: string;
  width?: number;
  height?: number;
};

export type TableProps<T, K extends keyof T> = {
  data: Array<T>;
  columns: Array<ColumnDefinitionType<T, K>>;
  tableTitel: string;
};

export type TableRowsProps<T, K extends keyof T> = {
  data: Array<T>;
  columns: Array<ColumnDefinitionType<T, K>>;
};

export type TableHeaderProps<T, K extends keyof T> = {
  columns: Array<ColumnDefinitionType<T, K>>;
};

export interface PairData {
  date?: string;
  high?: number;
  low?: number;
  open: number;
  close?: number;
  difference?: number;
}

export interface StylesTableProps {
  color?: string;
  align?: 'center' | 'left' | 'right' | 'justify' | 'char' | 'undefined';
  height?: string;
}
