import { FC } from 'react';
// import { css } from '@emotion/react';
import { ButtonStyled } from './styles';
import { AtomButtonTypes } from './types';
// import AtomLoader from '../AtomLoader';

const Animation = {
  whileHover: { scale: 1.03, transition: { duration: 0.2 } },
  whileTap: { scale: 0.8, opacity: 0.8 }
};

const AtomButton: FC<AtomButtonTypes> = (props) => {
  const { children, disabled, refObject } = props;
  return (
    <ButtonStyled ref={refObject} {...(disabled ? {} : Animation)} {...props}>
      <>{children || `Text Default`}</>
    </ButtonStyled>
  );
};
export default AtomButton;
