import { theme } from '@/styles/globalstyles';

export const typeAlert = {
  error: {
    color: theme?.darkMode?.red
    // icon: '',
  },
  success: {
    color: '#01a977'
    // icon: '',
  },
  warning: {
    color: '#f75b13'
    // icon: '',
  },
  info: {
    color: '#4b56f0'
    // icon: '',
  }
};
