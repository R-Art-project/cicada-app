import { AnimatePresence } from 'framer-motion';
import {
  FC,
  useState,
  Dispatch,
  SetStateAction,
  useRef,
  useEffect
} from 'react';
import { css } from '@emotion/react';
import { AlertProps, AlertContextProps } from './types';
import { typeAlert } from './colors';
import { ContextAlert } from './context';
import AtomWrapper from '../AtomWrapper';
import { theme } from '@/styles/globalstyles';

const Alert: FC<AlertContextProps> = ({ children, time, customcss }) => {
  const [alert, setAlert] = useState<AlertProps[]>([]);

  return (
    <ContextAlert.Provider value={{ alert, setAlert }}>
      <AnimatePresence mode="wait">
        {alert?.length > 0 && (
          <AtomWrapper
            maxWidth="100vw"
            width="auto"
            position="fixed"
            zIndex="9999"
            padding="10px"
            alignItems="flex-end"
            justifyContent="center"
            customcss={css`
              top: 0;
              right: 0;
              transform: translateX(-10%);
              animation: all 0.5s ease-in-out;
              ${customcss}
            `}
          >
            <AnimatePresence>
              {alert?.map((item) => (
                <UniqueAlert
                  key={item.id}
                  {...item}
                  setAlert={setAlert}
                  time={time}
                />
              ))}
            </AnimatePresence>
          </AtomWrapper>
        )}
      </AnimatePresence>
      {children}
    </ContextAlert.Provider>
  );
};

export default Alert;

type UniqueAlertType = AlertProps & {
  setAlert: Dispatch<SetStateAction<AlertProps[]>>;
  time: number;
};

const DefaultAnimation = {
  customcss: css`
    animation: AlertAnimation 0.8s forwards;
    @keyframes AlertAnimation {
      0% {
        transform: translateX(3000px);
      }
      60% {
        transform: translateX(-25px);
      }
      75% {
        transform: translateX(10px);
      }
      90% {
        transform: translateX(-5px);
      }
    }
  `
};
const UniqueAlert: FC<UniqueAlertType> = (props) => {
  const { id, message, setAlert, type, time = 3000 } = props;
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (time > 0) {
        setAlert((a) => a.filter((item) => item.id !== id));
      }
    }, time);
    return () => clearTimeout(timer);
  }, [alert, time]);

  return (
    <AtomWrapper
      refObject={ref}
      {...DefaultAnimation}
      customcss={css`
        max-width: max-content;
        margin: 0px 0px 20px 0px;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        padding: 20px 25px;
        border-radius: 10px;
        background-color: ${type && typeAlert[type].color};
        span {
          color: ${theme?.darkMode?.white};
          font-family: 'flama';
        }
        @media (max-width: 500px) {
          max-width: 80vw;
        }
      `}
    >
      <span>{message}</span>
    </AtomWrapper>
  );
};
