import { CurrentRateType } from '@/features/currentRaite/currentRaiteSlice';
import { PairStateType } from '@/features/pairs/pairSlice';
import { theme } from '@/styles/globalstyles';
import { css } from '@emotion/react';
import { useSelector } from 'react-redux';
import AtomWrapper from '../atoms/AtomWrapper';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  ArcElement
} from 'chart.js';
import { FC } from 'react';
import { ChatsProps } from './types';

ChartJS.register(
  ArcElement,
  Tooltip,
  Legend,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title
);

export const ChartComponent: FC<ChatsProps> = (props) => {
  const { labels, datasets } = props;

  return (
    <AtomWrapper
      customcss={css`
        background-color: ${theme?.darkMode?.black};
        width: 100% !important;
        height: 100%;
        min-height: 400px;
        border-radius: 10px;
        filter: drop-shadow(5px 5px 8px rgba(29, 28, 27, 0.8));
        padding: 20px;
        margin-top: 20px;
        overflow: auto;
      `}
    >
      <Line
        data={{
          labels: labels,
          datasets: datasets
        }}
        options={{
          responsive: true,
          maintainAspectRatio: false,
          scales: {
            y: {
              stacked: true,
              grid: {
                display: true,
                color: theme?.darkMode?.gray
              }
            },
            x: {
              grid: {
                display: true,
                color: theme?.darkMode?.white
              }
            }
          },
          plugins: {
            legend: {
              position: 'top' as const
            },
            title: {
              display: false
            }
          }
        }}
      />
    </AtomWrapper>
  );
};
