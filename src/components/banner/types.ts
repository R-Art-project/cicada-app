import { MotionProps } from 'framer-motion';

export interface BannerTypes extends MotionProps {
  color?: string;
  currentExchangePoint?: number | string;
  currentExchangeDate?: string;
  maxRateToday?: number | any;
  minRateToday?: number | any;
}
