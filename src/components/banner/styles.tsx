import { theme } from '@/styles/globalstyles';
import { css } from '@emotion/react';
import styled from '@emotion/styled';
import { motion } from 'framer-motion';
import { BannerTypes } from './types';

export const BannerContainer = styled(motion.div)`
  background-color: ${theme?.darkMode?.black};
  border: ${`1px solid ${theme?.darkMode?.gray}`};
  width: 100%;
  height: 71px;
  padding: 12px 0;
  display: flex;
  justify-content: space-between;

  p {
    color: ${theme?.darkMode?.white};
  }

  @media (max-width: 1011px) {
    flex-direction: column;
    height: auto;
    row-gap: 1em;
  }
`;

export const Label = styled(motion.h6)`
  color: ${theme?.darkMode?.gray};
  font-family: 'Flama';
  font-size: 12px;
`;

export const Value = styled(motion.span)<BannerTypes>`
  font-family: 'Flama';
  color: ${({ color }) => color || theme?.darkMode?.white};
  font-size: 16px;
`;
