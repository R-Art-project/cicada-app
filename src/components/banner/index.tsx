import { PairStateType } from '@/features/pairs/pairSlice';
import { theme } from '@/styles/globalstyles';
import { css } from '@emotion/react';
import { FC } from 'react';
import { useSelector } from 'react-redux';
import AtomWrapper from '../atoms/AtomWrapper';
import { BannerContainer, Label, Value } from './styles';
import { BannerTypes } from './types';

export const Banner: FC<BannerTypes> = (props) => {
  const { color } = props;
  const {
    currentExchangePoint,
    currentExchangeDate,
    maxRateToday,
    minRateToday
  } = props;
  const pair = useSelector(
    (state: PairStateType) => state?.pairs?.selectedpair
  );

  const bannerData = [
    {
      label: 'Highest Exchange-Rate Today',
      id: '2',
      value: maxRateToday
    },
    {
      label: 'Lowest Exchange-Rate Today',
      id: '3',
      value: minRateToday
    },
    {
      label: 'Last Update (UTC)',
      id: '4',
      value: currentExchangeDate
    }
  ];

  return (
    <BannerContainer>
      <AtomWrapper
        customcss={css`
          width: 40%;
          height: 100%;
          display: flex;
          flex-direction: row;
          flex-wrap: nowrap;
          justify-content: space-between;
          @media (max-width: 1011px) {
            width: 100%;
            gap: 10px;
          }
        `}
      >
        <AtomWrapper
          customcss={css`
            display: flex;
            height: 100%;
            width: auto;
            flex-direction: column;
            flex-wrap: nowrap;
            justify-content: space-between;
            align-items: flex-start;
            align-content: stretch;
            border-left: 2px solid ${color};
            padding-left: 12px;
          `}
        >
          <Label>Currency Pair</Label>
          <Value>{pair?.label}</Value>
        </AtomWrapper>
        <AtomWrapper
          customcss={css`
            display: flex;
            height: 100%;
            width: auto;
            flex-direction: column;
            flex-wrap: nowrap;
            justify-content: space-between;
            align-items: flex-end;
            align-content: stretch;
            border-right: ${`1px solid ${theme?.darkMode?.gray}`};
            padding-right: 20px;
          `}
        >
          <Label>Current Exchange-Rate Value</Label>
          <Value color={color}>{currentExchangePoint}</Value>
        </AtomWrapper>
      </AtomWrapper>
      <AtomWrapper
        customcss={css`
          display: flex;
          height: 100%;
          width: 70%;
          flex-direction: row;
          justify-content: space-between;
          padding-left: 20px;
          gap: 10px;
          @media (max-width: 1011px) {
            width: 100%;
          }
          @media (max-width: 679px) {
            flex-direction: column;
            align-items: center;
          }
        `}
      >
        {bannerData.map((x) => (
          <AtomWrapper
            key={x?.id}
            customcss={css`
              display: flex;
              height: 100%;
              width: auto;
              flex-direction: column;
              flex-wrap: nowrap;
              justify-content: space-between;
              align-items: flex-end;
              align-content: stretch;
              padding-right: 20px;
              border-right: ${`1px solid ${theme?.darkMode?.gray}`};
              @media (max-width: 679px) {
                border-right: none;
                justify-content: center;
                padding-right: 0;
                align-items: center;
                border-top: ${`1px solid ${theme?.darkMode?.gray}`};
              }
            `}
          >
            <Label>{x?.label}</Label>
            <Value>{x?.value}</Value>
          </AtomWrapper>
        ))}
      </AtomWrapper>
    </BannerContainer>
  );
};
