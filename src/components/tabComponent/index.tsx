import { theme } from '@/styles/globalstyles';
import { css } from '@emotion/react';
import { FC, useEffect, useState } from 'react';
import AtomButton from '../atoms/AtomButtom';
import AtomWrapper from '../atoms/AtomWrapper';
import { TabContainer, TabHeader } from './styles';
import { TabComponentType } from './types';

export const TabComponent: FC<TabComponentType> = (props) => {
  const { children, tabs, movilVersion } = props;
  const [activeTab, setActiveTab] = useState<number>(0);
  const [open, setOpen] = useState<boolean>(false);
  const [width, setWidth] = useState<number>(0);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setWidth(window.innerWidth);
      window.addEventListener('resize', () => {
        setWidth(window.innerWidth);
      });
    }
  }, []);

  return (
    <TabContainer {...props}>
      {movilVersion && width <= 544 ? (
        <>
          <AtomButton
            type="button"
            onClick={() => setOpen(!open)}
            customcss={css`
              background-color: ${`${theme?.darkMode?.lightBlue}a9`};
              border-radius: 5px 0 0 5px;
              color: ${theme?.darkMode?.black};
              padding: 10px 15px;
              position: fixed;
              top: 20px;
              right: ${open ? `50vw` : 0};
              z-index: 999;
            `}
          >
            {open ? '>' : '<'}
          </AtomButton>
          {open && (
            <AtomWrapper
              customcss={css`
                height: 100%;
                width: 50vw;
                background-color: ${`${theme?.darkMode?.lightBlue}a9`};
                position: fixed;
                right: 0;
                top: 0;
                bottom: 0;
                z-index: 999;
                display: flex;
                flex-direction: column;
                flex-wrap: nowrap;
                justify-content: flex-start;
                align-items: stretch;
                align-content: stretch;
                padding: 2em;
              `}
            >
              <TabHeader {...props}>
                {tabs?.map((p, i) => {
                  function handleClick() {
                    p?.onClick();
                    setActiveTab(i);
                    setOpen(!open);
                  }
                  return (
                    <AtomButton
                      key={p?.id}
                      type="button"
                      onClick={() => handleClick()}
                      customcss={css`
                        width: 100%;
                        margin-bottom: 15px;
                        background-color: ${activeTab === i
                          ? theme?.darkMode?.primary
                          : theme?.darkMode?.secundary};
                        border-top: ${activeTab === i
                          ? `2px solid ${theme?.darkMode?.yelow}`
                          : 'none'};
                        border-radius: 0;
                        color: ${activeTab === i
                          ? theme?.darkMode?.white
                          : theme?.darkMode?.gray};
                        &:hover {
                          background-color: ${theme?.darkMode?.primary};
                          color: ${theme?.darkMode?.white};
                        }
                      `}
                    >
                      {p?.label}
                    </AtomButton>
                  );
                })}
              </TabHeader>
            </AtomWrapper>
          )}
        </>
      ) : (
        <TabHeader {...props}>
          {tabs?.map((p, i) => {
            function handleClick() {
              p?.onClick();
              setActiveTab(i);
            }
            return (
              <AtomButton
                key={p?.id}
                type="button"
                onClick={() => handleClick()}
                customcss={css`
                  background-color: ${activeTab === i
                    ? theme?.darkMode?.primary
                    : theme?.darkMode?.secundary};
                  border-top: ${activeTab === i
                    ? `2px solid ${theme?.darkMode?.lightBlue}`
                    : 'none'};
                  border-radius: 0;
                  margin-right: 9px;
                  color: ${activeTab === i
                    ? theme?.darkMode?.white
                    : theme?.darkMode?.gray};
                  &:hover {
                    background-color: ${theme?.darkMode?.primary};
                    color: ${theme?.darkMode?.white};
                  }
                `}
              >
                {p?.label}
              </AtomButton>
            );
          })}
        </TabHeader>
      )}
      {children}
    </TabContainer>
  );
};
