import { SerializedStyles } from '@emotion/react';
import { PointerEventHandler, ReactElement, RefObject } from 'react';
import { AtomButtonTypes } from '../atoms/AtomButtom/types';

export type TabComponentType = {
  id?: string;
  label?: string;
  movilVersion?: boolean;
  tabs: PairsType;
  marginHeader?: string;
  backgroundImage?: string;
  backgroundColorButton?: string;
  maxHeight?: string;
  children?: React.ReactNode;
  onMouseEnter?: any;
  onMouseLeave?: any;
  alignItems?: 'center' | 'flex-start' | 'flex-end';
  flexDirection?: 'column' | 'row' | 'row-reverse' | 'column-reverse';
  justifyContent?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
  flexWrap?: 'nowrap' | 'wrap';
  mixBlendMode?:
    | 'normal'
    | 'multiply'
    | 'screen'
    | 'overlay'
    | 'darken'
    | 'lighten'
    | 'color-dodge'
    | 'color-burn'
    | 'hard-light'
    | 'soft-light'
    | 'difference'
    | 'exclusion'
    | 'hue'
    | 'saturation'
    | 'color'
    | 'luminosity';
  shadow?: boolean;
  maxWidth?: string;
  minHeight?: string;
  backgroundColor?: string;
  backgroundSize?: string;
  borderRadius?: string;
  margin?: string;
  padding?: string;
  border?: string;
  outline?: string;
  width?: string;
  height?: string;
  zIndex?: string;
  overflowX?: string;
  onSubmit?: () => void;
  position?: 'relative' | 'absolute' | 'fixed' | 'sticky';
  cursor?:
    | 'pointer'
    | 'default'
    | 'text'
    | 'wait'
    | 'move'
    | 'not-allowed'
    | 'help'
    | 'zoom-in'
    | 'zoom-out'
    | 'context-menu'
    | 'cell'
    | 'crosshair'
    | 'vertical-text'
    | 'alias'
    | 'progress'
    | 'no-drop'
    | 'copy'
    | 'grab'
    | 'grabbing'
    | 'all-scroll'
    | 'col-resize'
    | 'row-resize'
    | 'n-resize'
    | 'e-resize'
    | 's-resize'
    | 'w-resize'
    | 'ne-resize'
    | 'nw-resize'
    | 'se-resize'
    | 'sw-resize'
    | 'ew-resize'
    | 'ns-resize'
    | 'nesw-resize'
    | 'nwse-resize'
    | 'zoom-in'
    | 'zoom-out'
    | 'grab'
    | 'grabbing'
    | 'custom';
  asWrapper?: string;
  as?: 'div' | 'form' | 'section' | 'li';
  refObject?: RefObject<any>;
  customcss?: SerializedStyles;
  onPointerDown?: PointerEventHandler<any>;
  dangerouslySetInnerHTML?: { __html: string };
};

export type PairsType = {
  id?: string;
  label?: string;
  onClick: () => void;
}[];
