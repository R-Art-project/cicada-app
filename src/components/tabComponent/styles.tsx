import { theme } from '@/styles/globalstyles';
import { css } from '@emotion/react';
import styled from '@emotion/styled';
import { motion } from 'framer-motion';
import { TabComponentType } from './types';

const TabStyled = (props: TabComponentType) => css`
  display: flex;
  width: ${props?.width || `100%`};
  max-width: ${props?.maxWidth || `100%`};
  border: ${props?.border || `none`};
  outline: ${props?.outline || `none`};
  min-height: ${props?.minHeight || `initial`};
  max-height: ${props?.maxHeight || `initial`};
  height: ${props?.height || `max-content`};
  flex-direction: ${props?.flexDirection || `column`};
  justify-content: ${props?.justifyContent || `center`};
  align-items: ${props?.alignItems || `flex-start`};
  background-image: ${props?.backgroundImage};
  background-position: center;
  background-size: ${props?.backgroundSize || `cover`};
  padding: ${props?.padding || `50px`};
  margin: ${props?.margin || `0px`};
  flex-wrap: ${props?.flexWrap || `nowrap`};
  ${props?.shadow && `box-shadow: 0px 10px 20px #00000029`};
  border-radius: ${props?.borderRadius || `0px`};
  ${`overflow-x:${props?.overflowX}`};
  z-index: ${props?.zIndex || `0`};
  position: ${props?.position || `static`};
  cursor: ${props?.cursor || `default`};
  mix-blend-mode: ${props?.mixBlendMode || `normal`};

  ${props?.customcss};
`;

export const TabContainer = styled(motion.div)<TabComponentType>`
  ${(props) => TabStyled(props)};
`;

export const TabHeader = styled(motion.div)<TabComponentType>`
  width: 100%;
  height: auto;
  margin: ${(props) => props?.marginHeader || '38px'};
`;
