import { createSlice } from '@reduxjs/toolkit';

export interface PairStateType {
  pairs: PairType;
}
export interface PairType {
  selectedpair: {
    id: string;
    label: string;
  };
}

const initialState: PairType = {
  selectedpair: {
    id: '',
    label: ''
  }
};
export const pairSlice = createSlice({
  name: 'selectedpair',
  initialState,
  reducers: {
    setPair: (state, action) => {
      state.selectedpair = action.payload;
    }
  }
});

export const { setPair } = pairSlice.actions;

export default pairSlice.reducer;
