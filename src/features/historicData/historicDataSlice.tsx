import { createSlice } from '@reduxjs/toolkit';

export interface HistoricSateDataType {
  pairData: HistoricDataType;
}
export interface HistoricDataType {
  historcData: {
    timeSeries: [
      {
        date: string;
        high: number;
        low: number;
        open: number;
        close: number;
        difference: number;
      }
    ];
  };
}

const initialState: HistoricDataType = {
  historcData: {
    timeSeries: [
      {
        date: '-',
        high: 0,
        low: 0,
        open: 0,
        close: 0,
        difference: 0
      }
    ]
  }
};

export const historicDataSlice = createSlice({
  name: 'pair-Data',
  initialState,
  reducers: {
    setHistoricPairData: (state, action) => {
      state.historcData['timeSeries'] = action.payload;
    }
  }
});

export const { setHistoricPairData } = historicDataSlice.actions;

export default historicDataSlice.reducer;
