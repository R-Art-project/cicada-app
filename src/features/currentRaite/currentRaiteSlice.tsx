import { createSlice } from '@reduxjs/toolkit';

export interface CurrentRateType {
  currentRate: unknown | any;
}

const initialState: CurrentRateType = {
  currentRate: {}
};
export const currentRateSlice = createSlice({
  name: 'currentRateData',
  initialState,
  reducers: {
    setCurrentRate: (state, action) => {
      if (
        typeof state.currentRate[`${action.payload?.pairId}`] === 'undefined'
      ) {
        state.currentRate[`${action.payload?.pairId}`] = [action.payload];
      } else {
        state.currentRate[`${action.payload?.pairId}`].push(action.payload);
      }
    }
  }
});

export const { setCurrentRate } = currentRateSlice.actions;

export default currentRateSlice.reducer;
