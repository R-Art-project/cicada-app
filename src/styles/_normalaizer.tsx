import { css, Global } from '@emotion/react';
import { FC } from 'react';

interface Props {
  scrollbarColor?: string;
  scrollbarWidth?: string;
}

const _NormalizerStyled: FC<Props> = ({ scrollbarColor, scrollbarWidth }) => (
  <Global
    styles={css`
      *,
      *::after,
      *::before {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
      }

      *:focus {
        outline: none;
      }

      html {
        width: 100%;
        overflow-x: hidden;
      }
      body {
        width: 100%;
        background-color: #1a202c;
      }
      @font-face {
        font-family: 'Flama Regular';
        src: url('public/fonts/Flama Regular.otf');
        font-style: normal;
        font-display: swap;
      }
      @font-face {
        font-family: 'Flama Bold';
        src: url('public/fonts/Flama-Bold.otf');
        font-style: bold;
        font-display: swap;
      }
      @font-face {
        font-family: 'Flama Medium';
        src: url('public/fonts/Flama-Medium.otf');
        font-style: medium;
        font-display: swap;
      }
      @font-face {
        font-family: 'Flama Book';
        src: url('public/fonts/FlamaBook.otf');
        font-style: bold;
        font-display: swap;
      }
      #__next {
        width: 100%;
        overflow-x: hidden;
      }
      blockquote,
      dl,
      dd,
      h1,
      h2,
      h3,
      h4,
      h5,
      h6,
      figure,
      p,
      pre {
        margin: 0;
      }

      h1,
      h2,
      h3,
      h4,
      h5,
      h6 {
        font-size: inherit;
        font-weight: inherit;
      }

      ol,
      ul {
        list-style: none;
        margin: 0;
        padding: 0;
      }

      textarea {
        resize: none;
        overflow: auto;
      }
      img {
        border-style: none;
      }

      button,
      input {
        overflow: visible;
      }

      button,
      input,
      optgroup,
      select,
      textarea {
        font-family: inherit;
        font-size: 100%;
        line-height: 1.15;
        margin: 0;
      }
      [type='button'],
      [type='reset'],
      [type='submit'],
      button {
        -webkit-appearance: button;
        -webkit-tap-highlight-color: transparent;
      }
      ::-webkit-scrollbar {
        width: ${scrollbarWidth ?? '5'};
      }
      ::-webkit-scrollbar-thumb {
        background-color: ${scrollbarColor || 'black'};
        border-radius: 20px;
      }
    `}
  />
);

export default _NormalizerStyled;
