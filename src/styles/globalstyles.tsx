import { FC } from 'react';
import NormalizerStyled from './_normalaizer';

type Props = {
  scrollbarColor?: string;
  scrollbarWidth?: string;
  children?: unknown;
};

const StylesGlobal: FC<Props> = ({
  children,
  scrollbarColor,
  scrollbarWidth
}) => (
  <>
    <NormalizerStyled
      scrollbarColor={theme?.darkMode?.primary}
      scrollbarWidth={'7px'}
    />
    {children}
  </>
);

export default StylesGlobal;

export const theme = {
  darkMode: {
    primary: '#2d3748',
    secundary: '#202837',
    background: '#1a202c',
    lightBlue: '#17c7ee',
    black: '#000000',
    yelow: '#ecc94b',
    white: '#fff',
    gray: '#909090',
    green: '#00ffc4',
    red: '#F56565'
  },
  ligthMode: {
    //...here we can define the light mode palette
  }
};

export const fonts = {
  bold: 'Flama Bold',
  book: 'Flama Book'
};
