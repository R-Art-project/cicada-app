import { setCurrentRate } from '@/features/currentRaite/currentRaiteSlice';
import { wsUrl } from '@/pages/api/api';

export function currentRaite(id: string, dispatch: any) {
  if (typeof window === 'undefined') return;
  function conect() {
    const socket = new WebSocket(`${wsUrl}`);
    // Connection opened
    socket.addEventListener('open', (event) => {
      socket.send(JSON.stringify({ action: 'subscribe', pair: `${id}` }));
    });
    // -----
    socket.onclose = (event) => {
      if (event.wasClean) {
        // If the connection drops cleanly, we wait 3 seconds and try to reconnect automatically.
        setTimeout(() => {
          conect();
        }, 3000);
      }
    };

    socket.addEventListener('message', (event) => {
      if (event?.data !== `Subscribed to ${id} (every 3 seconds)`) {
        const today = new Date();
        const now = today.toLocaleString();

        const value = {
          point: JSON?.parse(event.data)?.point,
          pairId: id,
          time: now
        };
        dispatch(setCurrentRate(value));
      }
    });
  }
  conect();
}
