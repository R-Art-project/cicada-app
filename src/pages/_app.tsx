import StylesGlobal from '@/styles/globalstyles';
import type { AppProps } from 'next/app';
import { store } from '../app/store';
import { Provider } from 'react-redux';
import Alert from '@/components/atoms/AtomAlert';
import { css } from '@emotion/react';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Alert
        customcss={css`
          top: 50px;
        `}
      >
        <StylesGlobal />
        <Component {...pageProps} />
      </Alert>
    </Provider>
  );
}
