export const baseUrl = `http://67.205.189.142:8000`;
export const wsUrl = `ws://67.205.189.142:8000/websockets/`;

export async function getPairs(params: string) {
  const data = await fetch(`${baseUrl}/${params}`).then((response) =>
    response.json()
  );

  return data;
}
